package test.Book;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import library.daos.BookHelper;
import library.entities.Book;

public class LowLevelIntergrationBookHelper {
	
	BookHelper sut_;

	@Before
	public void setUp() throws Exception {
		sut_ = new BookHelper();
	}

	@Test
	public void testMakeBook() {
		
		Book temp = (Book) sut_.makeBook("author1", "title1", "callNo1", 1);
		
		assertEquals(temp, new Book("author1", "title1", "callNo1", 1));
	}

}
