package test.Book;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import library.entities.Book;
import library.entities.Loan;
import library.entities.Member;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.ILoan;


public class LowLevelIntegrationBook {
	
	
	Book sut_;	
	String author_;
	String title_;
	String callNumber_;
	int id_;
	ILoan loan_;
	EBookState state_;
	Member member_;

	@Before
	public void setUp() throws Exception {
		
		author_ = "author1";
		title_ = "title1";
		callNumber_ = "callNo1";
		id_ = 1;
		member_ = new Member("Test","user","12345678","testUser@library.com",1);
		sut_ = new Book(author_,title_,callNumber_,id_);
		loan_ = new Loan(sut_,member_,new Date(),new Date(System.currentTimeMillis()));
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBorrow() {
		sut_.borrow(loan_);
		
		assertEquals(loan_, sut_.getLoan());
		assertEquals(EBookState.ON_LOAN,sut_.getState());		
	}

	@Test
	public void testReturnBook() {
		sut_.borrow(loan_);
		sut_.returnBook(false);
		
		assertEquals(EBookState.AVAILABLE,sut_.getState());
		assertEquals(null,sut_.getLoan());
		
		sut_.borrow(loan_);
		sut_.returnBook(true);
		
		assertEquals(EBookState.DAMAGED,sut_.getState());
		assertEquals(null,sut_.getLoan());	
	}
	
}
