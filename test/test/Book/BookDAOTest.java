package test.Book;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import library.daos.BookDAO;
import library.entities.Book;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

public class BookDAOTest {	
	
	BookDAO sut_;
	
	int nextId_;
	Map<Integer,IBook> bookMap_;
	IBookHelper helper_;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		nextId_ = 1;
		//bookMap_.put(0, new Book(null, null, null, nextId_));
		helper_ = mock(IBookHelper.class);
		sut_ = new BookDAO(helper_);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddBook() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		Book testBook1 = (Book) sut_.addBook("author1", "title1", "callNo1");		
		
		verify(helper_).makeBook("author1", "title1", "callNo1", 1);
		assertTrue(sut_.getBookMap().containsValue(testBook1));
		assertEquals(2,sut_.getNextID());		
		
		Book testBook2 = (Book) sut_.addBook("author2", "title2", "callNo2");
		
		verify(helper_).makeBook("author2", "title2", "callNo2", 2);
		assertTrue(sut_.getBookMap().containsValue(testBook2));
		assertEquals(3,sut_.getNextID());
		
		assertEquals(2,sut_.getBookMap().size());
		
	}

	@Test
	public void testGetBookByID() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		when(sut_.helper_.makeBook("author3", "title3", "callNo3", 3)).thenReturn((IBook)new Book("author3", "title3", "callNo3",  3));
		when(sut_.helper_.makeBook("author4", "title4", "callNo4", 4)).thenReturn((IBook)new Book("author4", "title4", "callNo4",  4));
		Book testBook1 = (Book) sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		Book testBook3 = (Book) sut_.addBook("author3", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		IBook testBookFromMap1 = sut_.getBookByID(1);
		IBook testBookFromMap2 = sut_.getBookByID(3);	
		
		assertEquals(testBook1.getAuthor(), testBookFromMap1.getAuthor());
		assertEquals(testBook1.getTitle(), testBookFromMap1.getTitle());
		assertEquals(testBook1.getCallNumber(), testBookFromMap1.getCallNumber());
		
		assertEquals(testBook3.getAuthor(), testBookFromMap2.getAuthor());
		assertEquals(testBook3.getTitle(), testBookFromMap2.getTitle());
		assertEquals(testBook3.getCallNumber(), testBookFromMap2.getCallNumber());
		
	}

	@Test
	public void testListBooks() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		when(sut_.helper_.makeBook("author3", "title3", "callNo3", 3)).thenReturn((IBook)new Book("author3", "title3", "callNo3",  3));
		when(sut_.helper_.makeBook("author4", "title4", "callNo4", 4)).thenReturn((IBook)new Book("author4", "title4", "callNo4",  4));
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.listBooks();
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertTrue(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author3", "title3", "callNo3",3)));
		assertTrue(temp.contains(new Book("author4", "title4", "callNo4",4)));
		
	}

	@Test
	public void testFindBooksByAuthor() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		when(sut_.helper_.makeBook("author1", "title3", "callNo3", 3)).thenReturn((IBook)new Book("author1", "title3", "callNo3",  3));
		when(sut_.helper_.makeBook("author4", "title4", "callNo4", 4)).thenReturn((IBook)new Book("author4", "title4", "callNo4",  4));
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author1", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByAuthor("author1");
		
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author1", "title3", "callNo3",3)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));
		
	}

	@Test
	public void testFindBooksByTitle() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		when(sut_.helper_.makeBook("author3", "title1", "callNo3", 3)).thenReturn((IBook)new Book("author3", "title1", "callNo3",  3));
		when(sut_.helper_.makeBook("author4", "title4", "callNo4", 4)).thenReturn((IBook)new Book("author4", "title4", "callNo4",  4));
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title1", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByTitle("title1");
		
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author3", "title1", "callNo3",3)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));
		
	}

	@Test
	public void testFindBooksByAuthorTitle() {
		when(sut_.helper_.makeBook("author1", "title1", "callNo1", 1)).thenReturn((IBook)new Book("author1", "title1", "callNo1",  1));
		when(sut_.helper_.makeBook("author2", "title2", "callNo2", 2)).thenReturn((IBook)new Book("author2", "title2", "callNo2",  2));
		when(sut_.helper_.makeBook("author3", "title1", "callNo3", 3)).thenReturn((IBook)new Book("author3", "title1", "callNo3",  3));
		when(sut_.helper_.makeBook("author4", "title4", "callNo4", 4)).thenReturn((IBook)new Book("author4", "title4", "callNo4",  4));
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title1", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByAuthorTitle("author3","title1");
		
		assertTrue(temp.contains(new Book("author3", "title1", "callNo3",3)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertFalse(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));
	}

}
