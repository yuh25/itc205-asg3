package test.Book;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import library.daos.BookDAO;
import library.daos.BookHelper;
import library.entities.Book;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

public class LowLevelIntergrationBookDao {	
	
	BookDAO sut_;	
	int nextId_;
	Map<Integer,IBook> bookMap_;
	IBookHelper helper_;

	@Before
	public void setUp() throws Exception {
		nextId_ = 1;
		helper_ = new BookHelper();
		sut_ = new BookDAO(helper_);		
	}

	@Test
	public void testAddBook() {		
		Book testBook1 = (Book) sut_.addBook("author1", "title1", "callNo1");		
		Book testBook2 = (Book) sut_.addBook("author2", "title2", "callNo2");
		
		assertTrue(sut_.getBookMap().containsValue(testBook1));		
		assertTrue(sut_.getBookMap().containsValue(testBook2));
		assertEquals(3,sut_.getNextID());		
		assertEquals(2,sut_.getBookMap().size());		
	}

	@Test
	public void testGetBookByID() {
		Book testBook1 = (Book) sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		Book testBook3 = (Book) sut_.addBook("author3", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		IBook testBookFromMap1 = sut_.getBookByID(1);
		IBook testBookFromMap2 = sut_.getBookByID(3);	
		
		assertEquals(testBook1.getAuthor(), testBookFromMap1.getAuthor());
		assertEquals(testBook1.getTitle(), testBookFromMap1.getTitle());
		assertEquals(testBook1.getCallNumber(), testBookFromMap1.getCallNumber());
		
		assertEquals(testBook3.getAuthor(), testBookFromMap2.getAuthor());
		assertEquals(testBook3.getTitle(), testBookFromMap2.getTitle());
		assertEquals(testBook3.getCallNumber(), testBookFromMap2.getCallNumber());		
	}

	@Test
	public void testListBooks() {
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.listBooks();		
		
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertTrue(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author3", "title3", "callNo3",3)));
		assertTrue(temp.contains(new Book("author4", "title4", "callNo4",4)));		
	}

	@Test
	public void testFindBooksByAuthor() {
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author1", "title3", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByAuthor("author1");
		
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author1", "title3", "callNo3",3)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));		
	}

	@Test
	public void testFindBooksByTitle() {
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title1", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByTitle("title1");
		
		assertTrue(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertTrue(temp.contains(new Book("author3", "title1", "callNo3",3)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));		
	}

	@Test
	public void testFindBooksByAuthorTitle() {
		sut_.addBook("author1", "title1", "callNo1");
		sut_.addBook("author2", "title2", "callNo2");
		sut_.addBook("author3", "title1", "callNo3");
		sut_.addBook("author4", "title4", "callNo4");
		
		List<IBook> temp = sut_.findBooksByAuthorTitle("author3","title1");
		
		assertTrue(temp.contains(new Book("author3", "title1", "callNo3",3)));
		assertFalse(temp.contains(new Book("author2", "title2", "callNo2",2)));
		assertFalse(temp.contains(new Book("author1", "title1", "callNo1",1)));
		assertFalse(temp.contains(new Book("author4", "title4", "callNo4",4)));
	}
}
