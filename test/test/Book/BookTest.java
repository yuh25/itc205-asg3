package test.Book;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import library.entities.Book;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.ILoan;

public class BookTest {
	
	Book sut_;	
	String author_;
	String title_;
	String callNumber_;
	int id_;
	ILoan loan_;
	EBookState state_;	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		author_ = "author1";
		title_ = "title1";
		callNumber_ = "callNo1";
		id_ = 1;
		loan_ = mock(ILoan.class);
		sut_ = new Book(author_,title_,callNumber_,id_);
	}

	@After
	public void tearDown() throws Exception {
		loan_ = null;
	}	
	
	@Test
	public void testBook() {		
		assertEquals(sut_.getAuthor(), author_);
		assertEquals(sut_.getTitle(), title_);
		assertEquals(sut_.getCallNumber(), callNumber_);
		assertEquals(sut_.getID(), id_);
	}

	@Test
	public void testSane() {
		Boolean test1 = sut_.sane(author_, title_, callNumber_, id_);
		Boolean test2 = sut_.sane(null, title_, callNumber_, id_);
		Boolean test3 = sut_.sane(author_, title_, callNumber_, 0);
		Boolean test4 = sut_.sane(null, title_, callNumber_, 0);

		assertTrue(test1);		
		assertFalse(test2);
		assertFalse(test3);
		assertFalse(test4);
	}

	@Test
	public void testBorrow() {
		sut_.borrow(loan_);
		
		assertEquals(loan_, sut_.getLoan());
		assertEquals(EBookState.ON_LOAN,sut_.getState());
	}


	@Test
	public void testGetLoan() {
			
		assertEquals(sut_.getLoan(),null);	
		
		sut_.borrow(loan_);		
		assertEquals(sut_.getLoan(),loan_);		
	}

	@Test
	public void testReturnBook() {
		
		sut_.borrow(loan_);
		sut_.returnBook(false);
		
		assertEquals(sut_.getLoan(),null);
		assertEquals(sut_.getState(),EBookState.AVAILABLE);
		
		sut_.borrow(loan_);
		sut_.returnBook(true);
		
		assertEquals(sut_.getLoan(),null);
		assertEquals(sut_.getState(),EBookState.DAMAGED);		
	}

	@Test
	public void testLose() {
		
		sut_.borrow(loan_);		
		sut_.lose();
		
		assertEquals(sut_.getState(),EBookState.LOST);
	}

	@Test
	public void testRepair() {
		sut_.borrow(loan_);		
		assertEquals(sut_.getState(),EBookState.ON_LOAN);
		
		sut_.returnBook(true);		
		assertEquals(sut_.getState(),EBookState.DAMAGED);
		
		sut_.repair();		
		assertEquals(sut_.getState(),EBookState.AVAILABLE);			

	}

	@Test
	public void testDispose() {		
		sut_.dispose();		
		assertEquals(sut_.getState(),EBookState.DISPOSED);
	}

}
