package library.daos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

public class BookDAO implements IBookDAO {
	
	int nextId_;
	Map<Integer,IBook> bookMap_;
	public IBookHelper helper_;
	
	
	public BookDAO(IBookHelper helper){
		if(helper == null){
			throw new IllegalArgumentException();
		}
		bookMap_ = new HashMap<Integer,IBook>();
		helper_ = helper;
		nextId_ = 1;
	}	

	@Override
	public IBook addBook(String author, String title, String callNo) {
		
		IBook temp = helper_.makeBook(author, title, callNo, nextId_);
		if(temp == null){
			throw new RuntimeException("helper returned null");		
		}
		bookMap_.put(nextId_, temp);
		nextId_++;		
		return temp;		
	}

	@Override
	public IBook getBookByID(int id) {
		
		if(bookMap_.containsKey(id)){
			return bookMap_.get(id);
		}
		
		return null;
	}

	@Override
	public List<IBook> listBooks() {
		return new ArrayList<IBook>(bookMap_.values());
	}

	@Override
	public List<IBook> findBooksByAuthor(String author) {
		List<IBook> bookList = this.listBooks();
		List<IBook> foundList = new ArrayList<IBook>();
		for (IBook book : bookList) {
		    if(book.getAuthor() == author){
		    	foundList.add(book);
		    }
		}		
		return foundList;
	}

	@Override
	public List<IBook> findBooksByTitle(String title) {
		List<IBook> bookList = this.listBooks();
		List<IBook> foundList = new ArrayList<IBook>();
		for (IBook book : bookList) {
		    if(book.getTitle() == title){
		    	foundList.add(book);
		    }
		}		
		return foundList;
	}

	@Override
	public List<IBook> findBooksByAuthorTitle(String author, String title) {
		List<IBook> bookList = this.listBooks();
		List<IBook> foundList = new ArrayList<IBook>();
		for (IBook book : bookList) {
		    if(book.getTitle() == title && book.getAuthor() == author){
		    	foundList.add(book);
		    }
		}		
		return foundList;
	}
	
	public int getNextID(){
		return nextId_;
	}
	
	public Map<Integer,IBook> getBookMap(){
		return bookMap_;		
	}

}
