package library;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import library.entities.Book;
import library.entities.Loan;
import library.interfaces.EBorrowState;
import library.interfaces.IBorrowUI;
import library.interfaces.IBorrowUIListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.ICardReaderListener;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.interfaces.hardware.IScannerListener;

public class BorrowUC_CTL implements ICardReaderListener, 
									 IScannerListener, 
									 IBorrowUIListener {
	
	private ICardReader reader;
	private IScanner scanner; 
	private IPrinter printer; 
	private IDisplay display;
	private int scanCount = 0;
	private IBorrowUI ui;
	private EBorrowState state; 
	private IBookDAO bookDAO;
	private IMemberDAO memberDAO;
	private ILoanDAO loanDAO;
	private int loanLimit = 5;
	
	private List<IBook> bookList;
	private List<ILoan> loanList;
	private IMember borrower;
	
	private JPanel previous;

	public BorrowUC_CTL(ICardReader reader, IScanner scanner, 
			IPrinter printer, IDisplay display,
			IBookDAO bookDAO, ILoanDAO loanDAO, IMemberDAO memberDAO ) {

		this.reader = reader;
		this.scanner = scanner;
		this.printer = printer;
		this.display = display;
		
		this.reader.addListener((ICardReaderListener)this);
		this.scanner.addListener((IScannerListener)this);
		
		this.bookDAO = bookDAO;
		this.memberDAO = memberDAO;
		this.loanDAO = loanDAO;		
		
		this.ui = new BorrowUC_UI(this);
		state = EBorrowState.CREATED;
	}
	
	public void initialise() {
		this.setState(EBorrowState.INITIALIZED);
		this.scanner.setEnabled(false);
		this.reader.setEnabled(true);
		this.previous = display.getDisplay();
		this.display.setDisplay((JPanel) ui, "Borrow UI");		
	}
	
	public void close() {
		display.setDisplay(previous, "Main Menu");
	}

	@Override
	public void cardSwiped(int memberID) {
		this.borrower = this.memberDAO.getMemberByID(memberID);        
		if(this.borrower != null){
			boolean overdue = this.borrower.hasOverDueLoans();
			boolean atLoanLimit = this.borrower.hasReachedLoanLimit();
			boolean overFineLimit = this.borrower.hasReachedFineLimit();
			boolean borrowing_restricted = overdue || atLoanLimit || overFineLimit;
			if(!borrowing_restricted){
				this.setState(EBorrowState.SCANNING_BOOKS);
				this.bookList = new ArrayList<IBook>();
				this.loanList = new ArrayList<ILoan>();
				this.reader.setEnabled(false);
				this.scanner.setEnabled(true);
				
			} else {
				this.setState(EBorrowState.BORROWING_RESTRICTED);
				this.reader.setEnabled(false);
				this.scanner.setEnabled(false);
				this.ui.displayErrorMessage("Member cannot borrow.");
			}
			String fName = this.borrower.getFirstName();
			String lName = this.borrower.getLastName();
			String phoneNo = this.borrower.getContactPhone();
			
			this.ui.displayMemberDetails(memberID, fName +" "+ lName, phoneNo);
			if (overdue) {
				this.ui.displayOverDueMessage();
			}
			if(atLoanLimit){
				this.ui.displayAtLoanLimitMessage();
			}
			if(this.borrower.hasFinesPayable()){
				this.ui.displayOutstandingFineMessage(this.borrower.getFineAmount());
			}
			if (overFineLimit) {
			    this.ui.displayOverFineLimitMessage(this.borrower.getFineAmount());
			}			
			this.ui.displayExistingLoan(this.buildLoanListDisplay(this.borrower.getLoans()));
			scanCount = this.borrower.getLoans().size();
		} else{
			this.ui.displayErrorMessage("Could not find member");		
		}
	}	
	
	@Override
	public void bookScanned(int barcode) {		
		this.ui.displayScannedBookDetails("");
		if(this.state != EBorrowState.SCANNING_BOOKS){
			throw new RuntimeException("Control Class incorrect state! " 
												+ this.state.toString());
		}		
		Book book = (Book) this.bookDAO.getBookByID(barcode);
		if(book == null){
			this.ui.displayErrorMessage("Could not find book");
		} else {
			if(book.getState() != EBookState.AVAILABLE){
				this.ui.displayErrorMessage("Book is not available to loan");			
			} else {			
				if(this.bookList.contains(book)){
					this.ui.displayErrorMessage("Book has already Scanned");
				} else {
					this.scanCount++;
					this.bookList.add(book);
					this.ui.displayScannedBookDetails(book.toString());
					Loan loan = (Loan) this.loanDAO.createLoan(this.borrower, book);
					this.loanList.add(loan);
					this.ui.displayPendingLoan(this.buildLoanListDisplay(this.loanList));
					if (this.scanCount >= this.loanLimit) {
						scansCompleted();
					}
				}
			}
		}
	}
	
	private void setState(EBorrowState state) {
		this.state = state;
		this.ui.setState(state);
	}

	@Override
	public void cancelled() {
		close();
	}
	
	@Override
	public void scansCompleted() {
		if(this.state != EBorrowState.SCANNING_BOOKS){
			throw new RuntimeException("Control Class incorrect state!" + this.state.toString());
		}
		this.setState(EBorrowState.CONFIRMING_LOANS);
		this.reader.setEnabled(false);
		this.scanner.setEnabled(false);
		this.ui.displayConfirmingLoan(this.buildLoanListDisplay(this.loanList));
	}

	@Override
	public void loansConfirmed() {
		if(this.state != EBorrowState.CONFIRMING_LOANS){
			throw new RuntimeException("Control Class incorrect state!" + this.state.toString());
		}
		this.setState(EBorrowState.COMPLETED);
		this.reader.setEnabled(false);
		this.scanner.setEnabled(false);
		for (ILoan loan : this.loanList) {
			this.loanDAO.commitLoan(loan);
		}
		this.printer.print(this.buildLoanListDisplay(this.loanList));
		this.close();
	}

	@Override
	public void loansRejected() {
		if(this.state != EBorrowState.CONFIRMING_LOANS){
			throw new RuntimeException("Control Class incorrect state! " + this.state.toString());
		}
		this.setState(EBorrowState.SCANNING_BOOKS);
		this.reader.setEnabled(false);
		this.scanner.setEnabled(true);
		this.bookList = new ArrayList<IBook>();
		this.loanList = new ArrayList<ILoan>();
		this.scanCount = this.borrower.getLoans().size();
		this.ui.displayScannedBookDetails("");
		this.ui.displayPendingLoan("");		
	}

	private String buildLoanListDisplay(List<ILoan> loans) {
		StringBuilder bld = new StringBuilder();
		for (ILoan ln : loans) {
			if (bld.length() > 0) bld.append("\n\n");
			bld.append(ln.toString());
		}
		return bld.toString();		
	}
	
	public EBorrowState getState(){
		return this.state;
	}
	
	public IMember getBorrower() {
		return borrower;
	}

	public int getScanCount() {
		return this.scanCount;
	}
	
	public void setUI(IBorrowUI ui){
		this.ui = ui;
	}
	
	public List<IBook> getBookList(){
		return this.bookList;
	}

	public List<ILoan> getLoanList(){
		return this.loanList;
	}

}
