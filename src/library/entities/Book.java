package library.entities;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;

public class Book implements IBook{
	
	String author_;
	String title_;
	String callNumber_;
	int id_;
	ILoan loan_;
	EBookState state_;
	
	public Book(String author, String title, String callNumber,int bookID){
		
		if(!this.sane(author,title,callNumber,bookID)){
			throw new IllegalArgumentException("One or more parameters are null, empty or less than 0");
		}
		
		author_ = author;
		title_= title;
		callNumber_ = callNumber;
		id_ = bookID;
		loan_ = null;
		state_ = EBookState.AVAILABLE;
	}

	public Boolean sane(String author, String title, String callNumber,int bookID){		
		if(author == null  || title == null || callNumber == null || bookID <= 0 ){
			return false;			
		} else if(author.isEmpty() || title.isEmpty() || callNumber.isEmpty() ){
			return false;
		}		
		return true;	
	}	

	@Override
	public void borrow(ILoan loan) {		
		if(state_ != EBookState.AVAILABLE){
			throw new RuntimeException("Book state is not AVAILABLE");
		}
		
		loan_ = loan;
		state_ = EBookState.ON_LOAN;
		
	}

	@Override
	public ILoan getLoan() {
		if(state_ == EBookState.ON_LOAN){
			return loan_;
		}
		return null;
	}

	@Override
	public void returnBook(boolean damaged) {		
		if(state_ != EBookState.ON_LOAN){
			throw new RuntimeException("Book state is not ON_LOAN");
		}		
		
		loan_ = null;
		if(damaged){
			state_ =  EBookState.DAMAGED;			
		} else {
			state_ =  EBookState.AVAILABLE;
		}		
		
	}

	@Override
	public void lose() {		
		if(state_ != EBookState.ON_LOAN){
			throw new RuntimeException("Book state is not ON_LOAN");
		}
		
		state_ = EBookState.LOST;		
	}

	@Override
	public void repair() {
		if(state_ != EBookState.DAMAGED){
			throw new RuntimeException("Book state is not DAMAGED");
		}
		state_ = EBookState.AVAILABLE;		
	}

	@Override
	public void dispose() {
		if(state_ == EBookState.AVAILABLE || state_ == EBookState.DAMAGED || state_ == EBookState.LOST){
			state_ = EBookState.DISPOSED;
		} else {		
			throw new RuntimeException("Book state is not AVAILABLE, DAMAGED or LOST");
		}
		
	}

	@Override
	public EBookState getState() {
		return state_;
	}

	@Override
	public String getAuthor() {
		return author_;
	}

	@Override
	public String getTitle() {
		return title_;
	}

	@Override
	public String getCallNumber() {
		return callNumber_;
	}

	@Override
	public int getID() {
		return id_;
	}	

	@Override
	public boolean equals(Object obj) {
		   boolean isEqual= false;

		    if (obj != null && obj instanceof Book){
		        isEqual = (author_ == ((Book)obj).getAuthor() && title_ == ((Book)obj).getTitle() && callNumber_ == ((Book)obj).getCallNumber() && id_ == ((Book)obj).getID());
		        isEqual = (isEqual && loan_ == ((Book)obj).getLoan() && state_ == ((Book)obj).getState());
		    }
		    return isEqual;
	}
	
	@Override
	public String toString(){
		return title_+" "+author_+" "+callNumber_;
	}
	
}
